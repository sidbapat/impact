# Impact

* Constructed a social platform as a web application using Node.js that provides access to knowledge and education by enabling students to connect with professors or experts on a topic of their interest.
*  Activated a server using Express.js that interacts with the MongoDB database using Mongoose to manipulate data on 
student and professor details based on a topic of interest.
* Rendered an interactive user interface that enables students to select topics of interest and teachers to enrol themselves.

## Description

The objective of this web application is to ensure that anyone, anywhere has access to knowledge and education. It aims to reduce the illiteracy rate at underdeveloped countries.

* This web application is a social platform that enables students and teachers connect with each other based on a topic/course of interest.
* People can enrol themselves as teachers/experts and specify the courses they are willing to teach.
* Likewise, people can enroll themselves as students and specify the courses they wish to learn.
* Both the student and teacher details are stored in the database.
* Students can then search for available professors based on the selected course/topic and using the contact information, they could connect with them offline and work/learn from them on selected topics.

### Execution

* MongoDB, Node.js, Express.js, Mongoose must be installed. I installed MongoDB and Node.js manually prior to the following.
* Create a main directory for the website. Within the directory, create a  server file and create another separate folder called "public" containing the static HTML, CSS files, Client side JavaScript code and other necessary images, ttf files as required by your website.
* Open a command prompt, traverse to the main directory and install latest npm:
```
npm install -g npm
``` 
* Initialise using the following:
```
npm init
```
* Be sure to specify your server code as the entry point by specifying the server code name before pressing 'Enter'. You may select the default options for others by simply pressing 'Enter'.
* Install Express.js and Mongoose using the following commands:
```
npm install express
npm install mongoose
```
* Once you have installed everything, you can run the application.
* Express.js is used to help construct a backend server for the web application.
* First run the server code (be sure to include the necessary expressions to import the express and mongoose packages. Use the server code for reference) using the command:
```
node server.js
``` 
* In the browser, use the url "localhost:8080" (The port for the server was specified as 8080). The HTML files should be displayed. Modify the get() and post() functions according to what you need to process and display to the client.
* The server code includes the expression for connecting to MongoDB at the beginning. For this project, as long as the server is running, the connection to MongoDB is always on. When the server is shut down, the connection is terminated. The interactions are done using mongoose. Be sure to modify the connections as per your convenience. To be on the safe side, have the MongoDB command prompt active or rather the MongoDB server active before running the application depending on how you use the database.
 
